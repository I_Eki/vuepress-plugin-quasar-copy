const { path } = require('@vuepress/utils')

module.exports = (options, context) => ({
  define() {
    const {
      copySelector,
      copyMessage,
      failedMessage,
      duration,
      showInMobile
    } = options

    return {
      COPY_SELECTOR: copySelector || ['div[class*="language-"] pre', 'div[class*="aside-code"] aside'],
      COPY_MESSAGE: copyMessage || 'Copy successfully!',
      FAILED_MESSAGE: failedMessage || 'Copy failed!',
      DURATION: duration || 3000,
      SHOW_IN_MOBILE: showInMobile || false
    }
  },

  clientAppSetupFiles: path.resolve(__dirname, './bin/clientAppSetupFiles.js')
})
